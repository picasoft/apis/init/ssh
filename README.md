# Jour 2 : Connexion à distance avec SSH

Ce document a pour but d'être un support de cours sur le thème de la connexion à distance avec SSH.

Le document est généré automatiquement à partir du fichier `main.tex` présent à la racine par une chaîne d'intégration. Une fois généré avec succès, il est publié automatiquement à [cette adresse](https://uploads.picasoft.net/api/ssh.pdf).

Ce diaporama est publié sous licence CC BY-SA
